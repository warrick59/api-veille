<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="style.css">
  <title>Connexion</title>
  <meta charset="UTF-8">
</head>

<div class="responsive">
  <!-- NAVBAR -->
  <nav class="navbar navbar-inverse" id="debug">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Pop'Veille</a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-collapse-3">
        <ul class="nav navbar-nav navbar-default navbar-right">
          <li><a href="index.php">Blog</a></li>
          <li>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</div>
  <!-- NAVBAR END -->

<!-- HEADER -->
<div class="jumbotron header">
  <h1>Connexion</h1>
</div>
<!-- HEADER END -->

<div class="container text-center">
  <div class="col-md-6 col-md-offset-3">
    <form action="connexion.php" method="post" class="form-signin">
      <h1 class="form-signin-heading text-muted">Se connecter</h1>
      <input type="text" class="form-control" placeholder="Username" name="username" required=""><br>
      <input type="password" class="form-control" placeholder="Password" name="password" required=""><br>
      <hr>
      <button class="btn btn-lg btn-primary btn-block" name="connexion" type="submit">Se connecter</button>
      <h2 class="text-center"><small>Ou</small></h2>
      <button class="btn btn-lg btn-primary btn-block" type="Register"> <a href="register.php"> Créer un compte </a> </button>
    </form>
  </div>
  </div>
  </div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


<?php
session_start();
if(isset($_POST["connexion"])){
  $username=$_POST["username"];
  $password=$_POST["password"];

  if($username&&$password) {
    $password=sha1($password);
    include 'handle.php';
    $query="SELECT * FROM users WHERE username='$username'&&password='$password'";
    $result = mysqli_query($handle,$query);
    if($handle->affected_rows > 0) {
      $query = "SELECT * FROM users WHERE username='$username'";
      $result = mysqli_query($handle,$query);
      $line=mysqli_fetch_array($result);
      $_SESSION['username']=$line['username'];
      $_SESSION['id']=$line['id'];
      header('Location:membre.php');

    } else {
      echo "Identifiant ou mot de passe incorecte";
    }

    } else {
    echo "Veuillez saisir tous les champs";
  }
}
?>
</body>
</html>
