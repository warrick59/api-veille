<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css"/>
  <link rel="stylesheet" href="style.css">
  <title>Ajout de la veille</title>
  <meta charset="UTF-8">
</head>
<body>

<?php
 session_start();
echo $_SESSION['username'];
?>

   <!-- NAVBAR -->
  <nav class="navbar navbar-inverse" id="debug">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Pop'Veille</a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-collapse-3">
        <ul class="nav navbar-nav navbar-default navbar-right">
          <li><a href="veille.php">Publier une veille</a></li>
          <li><a href="index.php">Blog</a></li>
          <li><a href="randomizer.php">Tirage au sort</a></li>
          <li><a href="#">Statistiques</a></li>
	  <li><a href="connexion.php">Connexion</a></li>
          <li><a href="deconnexion.php">Deconnexion <span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></li>
          <li>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- NAVBAR END -->

 <!-- HEADER -->
 <div class="jumbotron header">
 <h1>Ajout de la Veille</h1>
 </div>
 <!-- HEADER END -->

 <div class="container text-center">
 <div class="boldtext">

<?php
    ini_set('display_errors', 'Off');
$info= pathinfo($_FILES['MAX_FILE_SIZE']['name']);
$extension_info=$info['extension'];
$extensions=array('odt','txt','pdf','html','jpg','jpeg');

if (in_array($extension_info, $extensions)) {
$name = $_FILES["MAX_FILE_SIZE"]["name"];
$tmp_name = $_FILES['MAX_FILE_SIZE']['tmp_name'];
$error = $_FILES['MAX_FILE_SIZE']['error'];
$location1 = '/var/www/html/fichiersveille/';
if  (move_uploaded_file($tmp_name, $location1.$name)){
  echo 'Uploaded';
  echo "<br>";
             }
              else {
		echo "<br>";
               echo "vous n'avez pas envoyé d'images";
	       echo "<br>";
               }
	include 'handle.php';
         $query="INSERT INTO `files` (files) VALUES ('$name')";
        $result= mysqli_query($handle,$query);
    if ($handle->affected_rows > 0) { 
	  echo "le fichier est bien enregistré.<br>";
	}
	else {
	  echo "une erreur est survenue";
	}
}

include 'handle.php';
$info= pathinfo($_FILES['fichiers']['name']);
$extension_info=$info['extension'];
$extensions=array('jpg','jpeg','png');
	if (in_array($extension_info, $extensions)) {
$name = $_FILES["fichiers"]["name"];
$tmp_name = $_FILES['fichiers']['tmp_name'];
$error = $_FILES['fichiers']['error'];
$location = '/var/www/html/photoveille/';
    if  (move_uploaded_file($tmp_name, $location.$name)){
      echo 'Uploaded';
      echo "<br>";
        }
         else {
           echo "vous n'avez pas envoyé d'images.";
           echo "<br>";
          }
	include 'handle.php';
        $query="INSERT INTO `img` (image) VALUES ('$name')";
        $result= mysqli_query($handle,$query);
         if ($handle->affected_rows > 0) {
	  echo "l'image est bien enregistré.<br>";
	  echo "<br>";
	}
	else {
	  echo "une erreur est survenue";
	}
}
?>

 <?php
$titre=$_POST["titre"];
$date=$_POST["date"];
$keywords=$_POST["keywords"];
$class=$_POST["classe"];
$id_user=$_SESSION["id"];
	{
	include 'handle.php';
  $query="INSERT INTO veilles (titre,date,keywords,id_user) VALUES ('$titre','$date','$keywords','$id_user')";
  $result= mysqli_query($handle,$query);
  $query="INSERT INTO `typectn` (lib_type) VALUES ('$class')";
  $result=mysqli_query($handle,$query);

       if ($handle->affected_rows > 0) {
	       echo "la veille a bien été crée.<br>";
       }
       else {
         echo "Une erreur est survenue lors de l'enregistrement de ".$_POST["titre"].".<br>";
         echo "Une erreur est survenue lors de l'enregistrement de ".$_POST["date"].".<br>";
         echo "Une erreur est survenue lors de l'enregistrement de ".$_POST["keywords"].".<br>";
       }
     }
     echo "Retour au <a href=\"index.php\">sommaire</a>.";
?>

</div>
</div>

</body>
</html>
