<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css"/>
  <link rel="stylesheet" href="style.css">
  <title>Membres</title>
  <meta charset="UTF-8">
</head>
<body>

  <div class="bold">
    <?php
    session_start();
    if ($_SESSION['username'] == true) {
      echo "Vous êtes connecter !";
    }
    if ($_SESSION['username'] == false) {
      header('Location:error403.php');
    }
    ?>
  </div>

  <!-- NAVBAR -->
  <nav class="navbar navbar-inverse" id="debug">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Pop'Veille</a>
      </div>
      <div class="collapse navbar-collapse" id="navbar-collapse-3">
        <ul class="nav navbar-nav navbar-default navbar-right">
          <li><a href="veille.php">Publier une veille</a></li>
          <li><a href="index.php">Blog</a></li>
          <li><a href="randomizer.php">Tirage au sort</a></li>
          <li><a href="membre.php">Membres</a></li>
          <li><a href="#">Statistiques</a></li>
          <li><a href="connexion.php">Connexion</a></li>
          <li><a href="deconnexion.php">Deconnexion <span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></li>
          <li>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- NAVBAR END -->


  <!-- HEADER -->
  <div class="jumbotron header">
    <h1>Membres</h1>
  </div>
  <!-- HEADER END -->

  <div class="container text-center">
    <div class="boldtext">
      <a href="veille.php">Ajouter une veille</a>
      <br>
      <br>
      <br>
      Listes des membres :
      <br>

      <?php
      include 'handle.php';
      $query = "SELECT prenom	FROM users";
      $result= mysqli_query($handle,$query);
      while($line = mysqli_fetch_array($result)) {
        echo $line['prenom']."<br>";
      }
      ?>

    </div>
  </div>
</body>
</html>
