<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="style.css">
  <title>Veilles PopSchool</title>
  <meta charset="UTF-8">
  </head>
  <body>

    <!-- NAVBAR -->
    <nav class="navbar navbar-inverse" id="debug">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Pop'Veille</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-3">
          <ul class="nav navbar-nav navbar-default navbar-right">
            <li><a href="veille.php">Publier une veille</a></li>
            <li><a href="index.php">Blog</a></li>
            <li><a href="randomizer.php">Tirage au sort</a></li>
            <li><a href="#">Statistiques</a></li>
            <li><a href="connexion.php">Connexion</a></li>
            <li><a href="deconnexion.php">Deconnexion <span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></li>
            <li>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- NAVBAR END -->



    <?php
    if(isset($_POST['submit'])) {

      $name=$_POST["name"];
      $f_name=$_POST["f_name"];
      $promo=$_POST["promo"];
      $username=$_POST["username"];
      $password=$_POST["password"];
      $repeat=$_POST["confirmpassword"];

      $champs_vide=array();
      if (empty($name)) {
        $champs_vide[]='Nom ';
      }
      if (empty ($f_name)) {
        $champs_vide[]='Prenom ';
      }
      if (empty ($promo)) {
        $champs_vide[]='Promo ';
      }
      if (empty ($username)) {
        $champs_vide[]='Pseudo ';
      }
      if (empty ($password)) {
        $champs_vide[]='Mot de passe ';
      }
      if (empty ($repeat)) {
        $champs_vide[]='Confirmer le mot de passe ';
      }

      if ($password != $repeat) {
        echo "Les 2 mots de passe sont différents";
      } else {
        if (empty ($champs_vide)) {
          $password=sha1($password);
          include 'handle.php';

          $query="SELECT * FROM users WHERE username= '$username'";
          $result = mysqli_query($handle,$query);
          if($result->num_rows == 0) {
            $query="INSERT INTO users(nom, prenom, promo, username, password) VALUES('$name','$f_name','$promo','$username','$password')";
            $result=mysqli_query($handle,$query);
            die("<div style='padding-left:240px; margin-top: 10px; margin-bottom:-10px'>Votre inscription a bien été pris en compte. <a href='connexion.php'>connectez vous</a></div>");
          } else {
            echo "Ce pseudo n'est pas disponible";
          }

        } else {
          echo '<div style="padding-left:150px; color:red; margin-bottom: -15px"><h4 style="padding-left:90px; padding-bottom:10px;">Les champs suivant manquent :</h4> <span style="text-align:center">" ' .implode($champs_vide). ' "</span></div>';
        }
      }
    }

    ?>

    <div id="fullscreen_bg" class="fullscreen_bg"/>

    <div class="container">
      <form action="#" method="post"><br>
        <h1 class="form-signin-heading text-muted">Inscription</h1>
        <input type="text" class="form-control" name="name" placeholder="Votre Nom" value="<?php if (isset($_POST['register'])){echo $_POST['nom'];} ?>">
        <input type="text" class="form-control" name="f_name" placeholder="Votre Prénom" value="<?php if (isset($_POST['register'])){echo $_POST['prenom'];} ?>">
        <select name="promo" tabindex="4" class="form-control">
          <option value="Ada Lovelace">Ada Lovelace</option>
          <option value="Alan Turing">Alan Turing</option>
        </select>
        <input type="text" class="form-control" name="username" placeholder="Votre pseudo" value="<?php if (isset($_POST['register'])){echo $_POST['username'];} ?>">
        <input type="password" class="form-control" name="password" placeholder="Votre mot de passe" value="<?php if (isset($_POST['register'])){echo $_POST['password'];} ?>">
        <input type="password" class="form-control" name="confirmpassword" placeholder="Confirmer le mot passe" value="<?php if (isset($_POST['register'])){echo $_POST['confirmpassword'];} ?>">
        <hr>
        <input class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="Register">
      </form>

    </div>

  </div>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>
</html>
