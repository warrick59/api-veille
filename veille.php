<!DOCTYPE HTML>
<html>
<head>
	<title>Insérer une veille</title>
	<meta charset="utf-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="style.css">
</head>
<body>

	<?php
	session_start();
	//echo $_SESSION['username'];
	if ($_SESSION['username'] == false) {
		header('Location:error403.php');
	}
	?>

	<!-- NAVBAR -->
	<nav class="navbar navbar-inverse" id="debug">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-3">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="index.php"><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Pop'Veille</a>
			</div>
			<div class="collapse navbar-collapse" id="navbar-collapse-3">
				<ul class="nav navbar-nav navbar-default navbar-right">
					<li><a href="veille.php">Publier une veille</a></li>
					<li><a href="index.php">Blog</a></li>
					<li><a href="randomizer.php">Tirage au sort</a></li>
					<li><a href="#">Statistiques</a></li>
					<li><a href="connexion.php">Connexion</a></li>
					<li><a href="deconnexion.php">Deconnexion <span class="glyphicon glyphicon-off" aria-hidden="true"></span></a></li>
					<li>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<!-- NAVBAR END -->

	<!-- HEADER -->
	<div class="jumbotron header">
		<h1>Publier une veille</h1>
	</div>
		<!-- HEADER-END -->

	<div class="boldtext">
		<form action="insert_veille.php" method="POST" enctype="multipart/form-data">
			<p>Titre de la veille: <input type="text" name="titre"></p>
			<p>Date de publication: <input type="date" name="date"></p>
			<p>Mots clef: <input type="keywords" name="keywords"></p>
			<p>Classe/Categorie de la veille: <input type="text" name="classe"></p>
			<input class="center-block" type="file" name="MAX_FILE_SIZE" value="5242880">
			<p>Choisissez un fichier avec une taille inférieure à 5 Mo.</p>
			<input class="center-block" type="file" name="fichiers">
			<p>Choisissez une image avec une taille inférieure à 5 Mo.</p>
			<input class="btn btn-default center-block" type="submit" value="Envoyer">
		</form>
		<br>
		<button type="button" class="btn btn-primary center-block"><a href="blog.php">Retour</a></button>
	</div>

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>
